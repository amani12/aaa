appServices.factory('ContactService', function ($http,$q,$timeout) {
    return{
        list: function () {
            var def = $q.defer();
            $http.get(options.api_url + "/contactlist/").success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject('error');
            }); 
            return def.promise;   
        },
        get: function (id) {
            var def = $q.defer();
            $http.get(options.api_url + "/contactlist/" + id).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject('error');
            }); 
            return def.promise;   
        },
        add: function (param) {
            var def = $q.defer();
            $http.post(options.api_url + '/contactlist/', param).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject(data);
            }); 
            return def.promise;
        },
        update: function (id, param) {
            var def = $q.defer();
            $http.put(options.api_url + '/contactlist/' + id, param).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject(data);
            }); 
            return def.promise;
        },
        remove: function (id) {
            var def = $q.defer();
            $http.delete(options.api_url + '/contactlist/' + id).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject(data);
            }); 
            return def.promise;
        }
    }
});
