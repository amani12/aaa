'use strict';
var app = angular.module('myApp', [
  'ngRoute',
  'appControllers',
  'appServices'
]);

var appControllers = angular.module('appControllers', []);
var appServices = angular.module('appServices', []);

var options = {};
// à changer si vous allez deployer l'apps sur mobile il faut deployer le serveur aussi
//ou tu peux mettre l'adresse ip de ton pc si l'ordinateur et le mobile 
//sont connecté au mem reseau dans la phase de développement bien sur
options.api_url  = "http://localhost:3000";


app.config(['$routeProvider','$locationProvider',
  function($routeProvider,$locationProvider) {
    $routeProvider.
      when('/', {
        controller: 'CtrlHome',
        templateUrl: 'parts/home.html'
      });
}]);