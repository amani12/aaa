appControllers.controller('CtrlHome', ['$scope','ContactService',
    function CtrlHome($scope, ContactService) {
        
        var refresh = function() {
            ContactService.list().then(function (data) {
                // body...
                $scope.contactlist = data;
                $scope.contact = "";
            }, function (data) {
                // error
            });
        };

        refresh();

        $scope.addContact = function() {
          console.log($scope.contact);
          ContactService.add($scope.contact).then(function (data) {
              console.log(data);
              refresh();
          }, function (data) {
              //error
          })
        };

        $scope.remove = function(id) {
          console.log(id);
          ContactService.remove(id).then(function (data) {
              refresh();
          }, function (data) {
              // body...
          });
        };

        $scope.edit = function(id) {
           ContactService.get(id).then(function (data) {
               // body...
                $scope.contact = data;
           }, function (data) {
               //err
           });
        };  

    $scope.update = function() {
      console.log($scope.contact._id);
      ContactService.update($scope.contact._id, $scope.contact).then(function (data) {
          // body...
        refresh();
      }, function (data) {
          // body...
      })
    };

    $scope.deselect = function() {
        $scope.contact = "";
    };

}]);
